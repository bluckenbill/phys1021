Exam 1 will take place Tuesday 2/19 for Dr. Tsankov's class and Wednesday 2/20 for Dr. Opferman's class during your normal lecture time. Topics covered will include

    Units and unit conversions
    Describing motion (Distance, displacement, velocity, speed, acceleration, motion diagrams, etc.)
    Vectors (Magnitude, direction, components, addition, subtraction, etc.)
    Newton's laws of motion (both understanding them and using them)
    Kinematic equations for constant acceleration

This approximately corresponds to everything covered in the course through the end of this upcoming week. 

Expect the exam to be a mixture of question formats (multiple choice, open problem solving, etc.) and a mixture of conceptual and mathematical problem solving questions. Be sure to understand all problems from the activities, homework, and lecture, but do not expect exam problems to be identical to problems that you have seen before.

A formula sheet will be provided with the exam. We will post the formula sheet on Canvas in advance of the exam as soon as it is ready.

Please be sure to bring a calculator and a pencil to the exam.

All students are expected to attend. If there is any emergency which prevents you from attending, contact your instructor *immediately* and be prepared to document your excuse. A makeup may or may not be given at the instructor's discretion.

Here is a checklist of some of the things you should be able to do by the exam:

    Identify SI units
    Convert between units
    Define and use the various terms to describe motion (position, displacement, distance, velocity, speed, acceleration, etc.)
    Switch back and forth between various representations of vectors (magnitude and direction vs. components vs. drawings, etc.)
    Add and subtract vectors (using components or visually using drawings)
    State and explain Newton's three laws of motion
    Use Newton's laws of motion to predict and explain the outcome of given situations
    Use Newton's 2nd law to solve problems (drawing and solving free body diagrams, setting up F=ma equations)
    Use Newton's 3rd law to solve problems (drawing and identifying 3rd law pairs, solving mutli-object problems)
    Use the kinematic equations to solve 1D and 2D motion problems (both with gravitational acceleration and other accelerations)

